SKRIBE := skribilo
LOUT   := lout -a
PS2PDF := ps2pdf
GARCH  := tla

# Notes
#
# *  `skribe-dbg' is Skribe 1.2b with a patch that fixes handling of
#    forward unresolved references (see the mailing list archives,
#    Nov. 2004 or so).
#
# *  `lout-dbg' is Lout 3.30 with a patch that fixes handling of
#    ligatures with the Latin Modern fonts (see
#    http://lists.planix.com/pipermail/lout-users/2005q1/003892.html).

# `includeres' is part of `psutils'.
PS_INCLUDERES := includeres

# Directory where the Lout engine (lout.skr) can be found.
#SKRIBE_LOUT_DIR := $(HOME)/src/skribe-lout

# `tla build-config build' adds `customs.skr' in `lib/mosaic'.
#SKRFLAGS := --eval "(set! *skribe-auto-load-alist* (cons '(lout . \"lout.skr\") *skribe-auto-load-alist*))" \
#            -I $(SKRIBE_LOUT_DIR) -I lib/mosaic -g3 -v5 -w5

SKRFLAGS := -I lib/mosaic -v 5 -w 5

docs := storage.skb
docs_html := $(docs:%.skb=%.html)
docs_pdf  := $(docs:%.skb=%.pdf)

lout_max_passes:=3


all: $(docs_pdf) # $(docs_html)

%.html: %.skb
	$(SKRIBE) $(SKRFLAGS) -u html4 -o $@ $<

%.lout: %.skb
	$(SKRIBE) $(SKRFLAGS) --target lout -o $@ $<

%.tex: %.skb
	$(SKRIBE) $(SKRFLAGS) -o $@ $<

%.ps: %.lout
	@echo -n "PostScripting... " && p=0 &&				\
	while [ $$p -lt $(lout_max_passes) ] &&				\
	[ `$(LOUT) -c $(<:.lout=) $< -o $@ 2>&1 | wc -l` -gt 0 ] ;	\
	do echo -n "... " && p=`expr $$p + 1` ; done ;			\
	if [ $$p -ge $(lout_max_passes) ] ; then			\
	echo "Max number of passes reached." &&				\
	$(LOUT) -c $(<:.lout=) $< -o $@ ; else echo ; fi

%.ps2: %.ps
	$(PS_INCLUDERES) < $^ > $@

%.pdf: %.ps2
	$(PS2PDF) $< $@

build-config:
	$(GARCH) build-config --link build

.PHONY: all build-config
